local options = {
	lsp_fallback = true,

	formatters_by_ft = {
		lua = { "stylua" },
		python = { "pylint" },
	},

  -- adding same formatter for multiple filetypes can look too much work for some
  -- instead of the above code you could just use a loop! the config is just a table after all!

	-- format_on_save = {
	--   -- These options will be passed to conform.format()
	--   timeout_ms = 500,
	--   lsp_fallback = true,
	-- },
}

local conform = require("conform").setup(options)

local linters = {}

for _, linter in ipairs(linters) do
	conform[linter].setup({
    lsp_fallback = true,
    enable = true,
    server = {
      name = linter,
      executable = linter,
    },
    lint = {
      enable = true,
      on_attach = true,
    },
	})
end

conform.pylint.setup({
  enable = true,
  server = {
    name = 'pyls',
    executable = 'pyls', -- Adjust if installed differently
  },
  lint = {
    enable = true,
    on_attach = true,
    severityMap = {
      error = "error",
      warning = "warning",
      info = "info",
    },
  },
})
