local configs = require("plugins.configs.lspconfig")
local on_attach = configs.on_attach
local capabilities = configs.capabilities

local lspconfig = require "lspconfig"
local servers = {
  -- lua lsp is included already within NvChad so it is not necessary to include here
  -- c
  "ccls",
  -- rust
  "rust_analyzer",
  -- haskell
  "hls",

  -- tex
  "texlab",
  -- web stack
  -- "html/css/json/typesctipt"
  "angularls",

  -- python
  "pyright",

  -- golang
  "gopls",

  "htmx",

  "vtsls", -- typescript
}

for _, lsp in ipairs(servers) do
	lspconfig[lsp].setup({
		on_attach = on_attach,
		capabilities = capabilities,
	})
end
