local M = {}

local function termcodes(str)
  return vim.api.nvim_replace_termcodes(str, true, true, true)
end

M.general = {
  n = {
    ["<leader>lf"] = {":!pylint %<CR>", opts = { buffer = true }},
  },
  t = {
    ["<ESC>"] = { termcodes "<C-\\><C-N>", "escape terminal mode" },
  },
}

return M
