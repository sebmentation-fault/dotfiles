local M = {}

-- Path to overriding theme and highlights files
local highlights = require "custom.highlights"

--M.plugins = {
--  user = require("custom.plugins"),
--
--  override = {
--    ["kyazdani42/nvim-tree.lua"] = override.nvimtree,
--    ["nvim-treesitter/nvim-treesitter"] = override.treesitter,
--    ["lukas-reineke/indent-blankline.nvim"] = override.blankline,
--    ["goolord/alpha-nvim"] = override.alpha,
--    ["williamboman/mason.nvim"] = override.mason,
--  },
--
--}

M.ui = {
  theme = "oxocarbon",
  theme_toggle = { "oxocarbon", "oxocarbon"},

  hl_override = highlights.override,
  hl_add = highlights.add,
}

M.plugins = "custom.plugins"

M.mappings = require("custom.mappings")

return M
