# Dotfiles Repository

## How I manage my dotfiles

1. Create a `.dotfiles` folder:
```bash
git init --bare $HOME/.dotfiles/
```

2. Create an alias (I call it dotfiles), which will pass in the correct arguments to git:
```bash
alias dotfiles="/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME"
```

3. Reload your shell so that the alias is read.

4. **Most** files in your home directory are not quite considered config files. Run this to hide these untracked files when `dotfiles status` is run.
```bash
dotfiles config --local status.showUntrackedFiles no
```

