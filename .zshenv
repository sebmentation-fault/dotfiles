export EDITOR=/usr/bin/nvim
export SUDO_EDITOR=/usr/bin/nvim
export VISUAL=/usr/bin/nvim
export BROWSER=/usr/bin/firefox
export TERMINAL=/usr/bin/alacritty
export CHROME_EXECUTABLE=/opt/google/chrome/chrome
export STARSHIP_CONFIG=$HOME/.config/starship/starship.toml
export XDG_CONFIG_HOME=$HOME/.config/
export GEM_HOME=$HOME/.gems/

export ANDRIOD_HOME=$HOME/Android/Sdk

# Prebunk's secret key:
export DJANGO_SECRET_KEY='d&**7v+q!pdso(ybfbz1*@9w4m^a2bro65d&9a-1g3l+4j6v82'
export DJANGO_DEBUG=True

# Used by inoculate
export GOOGLE_APPLICATION_CREDENTIALS='/home/user/Development/FakeNewsInoculationGame/secret/firebase-admin-key.json'

# Language
export LANG=en_GB.UTF-8

# Add certain directories to PATH
export PATH="$HOME/.local/scripts:$PATH"
export PATH="$HOME/.local/bin:$PATH"
export PATH="$HOME/.config/rofi/scripts:$PATH"
export PATH="$HOME/.cargo/bin:$PATH"
export PATH="/opt/flutter/bin:$PATH"
export PATH="$HOME/.pub-cache/bin:$PATH"
export PATH="$ANDRIOD_HOME/tools:$PATH"
export PATH="$ANDRIOD_HOME/tools/bin:$PATH"
export PATH="$ANDRIOD_HOME/platform-tools:$PATH"
export PATH="$HOME/go/bin:$PATH"
export PATH="$HOME/.gems/bin:$PATH"
export PATH="$HOME/.local/share/gem/ruby/3.0.0/bin:$PATH"

