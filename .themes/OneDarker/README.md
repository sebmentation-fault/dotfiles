# Thanks to Siduck for this Colorscheme

The author of this colorscheme is [Siduck (*link to his github page*)](https://github.com/siduck/dotfiles/tree/master/gtk/siduck-onedark).

My (Seb) version may not be as up to date as his own. So it is recommended to clone his dotfiles project manually.

